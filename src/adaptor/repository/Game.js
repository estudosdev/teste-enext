class Game {
  constructor(line = '') {
    this.players = new Map();
    this.total_kills = 0;
    this.line = line;
  }

  add(line) {
    return new Game(line);
  }

  addKill() {
    this.total_kills += 1;
  }

  getPlayerById(id) {
    if (this.players.has(id)) {
      return this.players.get(id);
    }
    return null;
  }

  newPlayer(player) {
    return this.players.set(player.id, player);
  }

  playersNames() {
    const result = [];
    this.players.forEach((player) => {
      result.push(player.username);
    });
    return result;
  }

  playersKills() {
    const result = {};
    this.players.forEach((player) => {
      result[player.username] = player.calcScore();
    });
    return result;
  }
}

module.exports = Game;
