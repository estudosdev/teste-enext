const path = require('path');

const configFile = {
  directory: path.join(__dirname, '../games.log'),
};

module.exports = configFile;
