class ParserDomain {
  constructor() {
    this.initialDatas();
  }

  initialDatas() {
    this.games = new Map();
    this.currentGame = 0;
    this.createObjectPlayerToRanking();
  }

  createObjectPlayerToRanking() {
    this.players = {};
    this.players.Isgalamido = 0;
    this.players.Oootsimo = 0;
    this.players['Dono da Bola'] = 0;
    this.players['Assasinu Credi'] = 0;
    this.players.Zeh = 0;
    this.players.Mal = 0;
  }

  incrementCurrentGame() {
    this.currentGame += 1;
  }
}

module.exports = ParserDomain;
