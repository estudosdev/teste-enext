const fs = require('fs');

class OperationParser {
  constructor(configFileLogs, parserService) {
    this.fileLogs = configFileLogs.directory;
    this.parserService = parserService;
  }

  readFile() {
    return fs.readFileSync(this.fileLogs, { encoding: 'utf8' });
  }

  deparaAction(lines) {
    const pattern = /^.{0,7}([a-z A-Z][^:]*)/;
    return lines.map((line) => {
      const action = line.match(pattern);
      if (action) {
        return this.executeAction(action[1], line);
      }
      return null;
    });
  }

  executeAction(action, line) {
    if (action === 'InitGame') {
      this.parserService.newGame(line);
    } else if (action === 'ClientConnect') {
      const newPlayer = this.parserService.newPlayer(line);
      this.parserService.addNewPlayerInTheGame(newPlayer);
    } else if (action === 'ClientUserinfoChanged') {
      this.parserService.updatePlayer(line);
    } else if (action === 'Kill') {
      this.parserService.killPlayer(line);
    }
  }

  getGameById(id) {
    this.execute();
    const games = Array.from(this.parserService.getGames());
    const game = games.find((_game, index) => index === id);
    if (game) {
      return {
        total_kills: game[1].total_kills,
        players: game[1].playersNames(),
        kills: game[1].playersKills(),
      };
    }
    return null;
  }

  getAll() {
    this.execute();
    return this.toObject();
  }

  playersRanked() {
    this.execute();
    const games = Array.from(this.parserService.getGames());
    games.map(game => this.parserService.switchRanking(game[1].playersKills()));
    return this.parserService.getRankedPlayers();
  }

  toObject() {
    const ret = {};
    const games = this.parserService.getGames();
    games.forEach((game, index) => {
      ret[`game_${index}`] = {
        total_kills: game.total_kills,
        players: game.playersNames(),
        kills: game.playersKills(),
      };
    });
    return ret;
  }

  execute() {
    const file = this.readFile();
    const linesFile = file.toString().split('\n');
    this.deparaAction(linesFile);
  }
}

module.exports = OperationParser;
