const configFileLogs = require('src-res/config/file');
const PlayerRepository = require('src/adaptor/repository/Player');
const GameRepository = require('src/adaptor/repository/Game');
const DomainParser = require('src/domain/Parser');
const DomainService = require('src/domain/services/Parser');
const OperationParser = require('src/application/Parser');

const newPlayerRepository = new PlayerRepository();
const newGameRepository = new GameRepository();
const newDomainParser = new DomainParser();
const newDomainService = new DomainService(newDomainParser, newGameRepository, newPlayerRepository);

const createOperationParser = () => new OperationParser(configFileLogs, newDomainService);

module.exports = createOperationParser;
