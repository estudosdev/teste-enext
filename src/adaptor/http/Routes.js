const { Router } = require('express');
const ParserController = require('./controller/ParserController');

const newParserController = new ParserController();

module.exports = () => {
  const router = Router();
  router.get('/games', newParserController.getAll);
  router.get('/games/:id', newParserController.getById);
  return router;
};
