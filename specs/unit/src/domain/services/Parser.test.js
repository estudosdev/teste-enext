const ParserService = require('src/domain/services/Parser');
const {
  createGameRepository,
  createPlayerRepository,
  createMockLine,
  createMockPlayer,
} = require('../support');

describe('DOMAIN :: SERVICES :: PARSER', () => {
  let parserService;

  describe('Create new game', () => {
    beforeEach((done) => {
      const mockDomainParser = {
        incrementCurrentGame: () => null,
        currentGame: 0,
        games: new Map(),
      };

      const mockGameRepository = {
        add: () => createGameRepository(),
      };

      parserService = new ParserService(mockDomainParser, mockGameRepository, {});
      done();
    });

    test('Should add a new game', () => {
      const newGame = parserService.newGame(createMockLine());
      expect(newGame).toBeDefined();
      expect(newGame.players).toBeDefined();
      expect(newGame.players).toBeInstanceOf(Map);
      expect(newGame.total_kills).toBeDefined();
    });
  });

  describe('New player', () => {
    beforeEach((done) => {
      const games = new Map();
      const currentGame = 1;
      const mockParamsNewGame = {
        newPlayer: () => new Map().set(createMockPlayer(1, 'Test', 10, 5)),
      };

      games.set(currentGame, mockParamsNewGame);

      const mockDomainParser = {
        incrementCurrentGame: () => null,
        games,
        currentGame,
      };

      const mockPlayerRepository = {
        add: () => createPlayerRepository(),
      };

      parserService = new ParserService(mockDomainParser, {}, mockPlayerRepository);
      done();
    });

    test('Should create a new player', () => {
      const newPlayer = parserService.newPlayer(createMockLine());
      expect(newPlayer.id).toBeDefined();
      expect(newPlayer.username).toBeDefined();
      expect(newPlayer.kills).toBeDefined();
      expect(newPlayer.deadsByWorld).toBeDefined();
    });

    test('Should add the player in the game', () => {
      const mockPlayer = createMockPlayer(1, 'Test', 10, 5);
      const players = parserService.addNewPlayerInTheGame(mockPlayer);
      expect(players.size).toBe(1);
    });
  });

  describe('Update player', () => {
    describe('When update is sucess', () => {
      beforeEach((done) => {
        const games = new Map();
        const currentGame = 1;
        const playerId = 1;

        const mockParamsNewGame = {
          getPlayerById: () => ({
            updateUsername: () => createMockPlayer(playerId, 'Player updated', 1050, 55),
          }),
        };

        games.set(currentGame, mockParamsNewGame);

        const mockDomainParser = {
          games,
          currentGame,
        };

        const mockPlayerRepository = {
          update: () => createPlayerRepository(),
          getPlayerId: () => playerId,
        };

        parserService = new ParserService(mockDomainParser, {}, mockPlayerRepository);
        done();
      });

      test('Should save the player changes', () => {
        const playerUpdated = parserService.updatePlayer(createMockLine());
        expect(playerUpdated.id).toBe(1);
        expect(playerUpdated.id).not.toBe(5);
        expect(playerUpdated.username).toBe('Player updated');
        expect(playerUpdated.username).not.toBe('Teste');
        expect(playerUpdated.kills).toBe(1050);
        expect(playerUpdated.deadsByWorld).toBe(55);
      });
    });

    describe('When player not is updated', () => {
      beforeEach((done) => {
        const games = new Map();
        const currentGame = 1;
        const playerId = 9999999999999999999999999999999999999;

        const mockParamsNewGame = {
          getPlayerById: () => ({
            updateUsername: () => null,
          }),
        };

        games.set(currentGame, mockParamsNewGame);

        const mockDomainParser = {
          games,
          currentGame,
        };

        const mockPlayerRepository = {
          update: () => createPlayerRepository(),
          getPlayerId: () => playerId,
        };

        parserService = new ParserService(mockDomainParser, {}, mockPlayerRepository);
        done();
      });

      test('You should not update the player', () => {
        const playerUpdated = parserService.updatePlayer(createMockLine());
        expect(playerUpdated).toBe(null);
      });
    });
  });
});
