/* eslint-disable no-console */
const createOperationParser = require('./Factory');

const operationParser = createOperationParser();

const ranking = new Promise((resolve, reject) => {
  const playerRanked = operationParser.playersRanked();
  if (!playerRanked) {
    reject(playerRanked);
  }
  resolve(playerRanked);
});

ranking
  .then(rank => console.log(rank))
  .catch(err => err);
