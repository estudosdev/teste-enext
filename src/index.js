const container = require('./Ioc');

const app = container.resolve('application');

app.initialize();
