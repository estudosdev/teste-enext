const USER_WORLD_ID = 1022;

class Player {
  constructor(line = '') {
    this.id = this.getPlayerId(line);
    this.setInitialDatas();
  }

  setInitialDatas() {
    this.username = '';
    this.kills = 0;
    this.deadsByWorld = 0;
  }

  getPlayerId(line) {
    const regex = /Client(Connect|UserinfoChanged): ([0-9]*)/;
    const playerId = line.match(regex);
    return playerId ? playerId[2] : 0;
  }

  add(line) {
    return new Player(line);
  }

  update(player, line) {
    if (player) {
      return player.updateUsername(line);
    }
    return player;
  }

  kill(parserService, line) {
    const currentGame = parserService.getCurrentGame();
    const pattern = /Kill: ([0-9]+) ([0-9]+)/;
    const players = line.match(pattern);

    if (players) {
      currentGame.addKill();
      // eslint-disable-next-line eqeqeq
      if (players[1] == USER_WORLD_ID) {
        let { deadsByWorld } = currentGame.players.get(players[2]);
        deadsByWorld += 1;
        currentGame.players.get(players[2]).deadsByWorld = deadsByWorld;
      } else {
        currentGame.players.get(players[1]).addKill();
      }
    }
  }

  calcScore() {
    const score = this.kills - this.deadsByWorld;
    return score < 0 ? 0 : score;
  }

  addKill() {
    this.kills += 1;
  }

  removeKill() {
    const killsToBeRemoved = this.kills > 0 ? 1 : 0;
    this.kills -= killsToBeRemoved;
  }

  updateUsername(line) {
    const pattern = /ClientUserinfoChanged: [0-9]* n\\(.*)\\t\\[0-9]+\\model/;
    // eslint-disable-next-line prefer-destructuring
    this.username = line.match(pattern)[1];
  }
}

module.exports = Player;
