const express = require('express');
const routes = require('./Routes');

class HttpServer {
  constructor({ configHttpServer }) {
    this.express = express();
    this.configHttpServer = configHttpServer;
    this.configRouter();
  }

  configRouter() {
    this.express.use(routes());
  }

  start() {
    try {
      const { PORT } = this.configHttpServer;
      this.express.listen(PORT);
    } catch (error) {
      throw error;
    }
  }
}

module.exports = HttpServer;
