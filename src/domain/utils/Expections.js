class ValidationError extends Error {
  constructor(message) {
    super();
    this.name = 'VALIDATION_ERROR';
    this.message = message;
  }
}

class NotFoundError extends Error {
  constructor(message) {
    super();
    this.name = 'NOT_FOUND_ERROR';
    this.message = message;
  }
}

module.exports = {
  NotFoundError,
  ValidationError,
};
