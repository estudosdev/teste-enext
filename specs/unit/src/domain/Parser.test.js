const DomainParser = require('src/domain/Parser');

describe('DOMAIN :: PARSER', () => {
  let domainParser;

  beforeAll((done) => {
    domainParser = new DomainParser();
    done();
  });

  describe('When the incrementCurrentGame function is invoked', () => {
    test('Initial value must be zero', () => {
      const { currentGame } = domainParser;
      expect(currentGame).toBe(0);
    });

    test('Must return one', () => {
      domainParser.incrementCurrentGame();
      const { currentGame } = domainParser;
      expect(currentGame).toBe(1);
    });
  });
});
