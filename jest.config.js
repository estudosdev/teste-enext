module.exports = {
  verbose: true,
  coverageReporters: ['text-summary', 'html'],
  testEnvironment: 'node',
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.js'],
};
