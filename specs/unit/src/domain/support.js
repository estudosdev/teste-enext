const fs = require('fs');
const configFileLogs = require('src-res/config/file');
const GameRepository = require('src/adaptor/repository/Game');
const PlyerRepository = require('src/adaptor/repository/Player');

const createGameRepository = (line = '') => new GameRepository(line);

const createPlayerRepository = (line = '') => new PlyerRepository(line);

const createMockLine = () => {
  const fileLogs = fs.readFileSync(configFileLogs.directory, { encoding: 'utf8' });
  const linesFile = fileLogs.toString().split('\n');
  return linesFile[0];
};

const createMockPlayer = (id = 0, username = '', kills = 0, deadsByWorld = 0) => ({
  id,
  username,
  kills,
  deadsByWorld,
});

module.exports = {
  createMockLine,
  createGameRepository,
  createPlayerRepository,
  createMockPlayer,
};
