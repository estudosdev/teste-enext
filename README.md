# Quake Log Parser

- These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

- [Pre-requisites]
  - [Docker]

## Docker

For development we're using docker with node 13.

## Getting Started

### Installing
```
    docker-compose run app ash
    npm install
```

Exit the container by typing:

```
    exit
```

## Running

### Start the App

```
    docker-compose up app
```

### Run Tests

```
    docker-compose run specs
```

### Run Script console

```
    docker-compose run app ash
    npm run games:list
    npm run players:rank
```

## Developing

The port of this appplication is predefined as `3009`

## Routes

** FETCH GAME BY ID **

*GET* ```/games/{id}```

*200* ```OK```

*Example*
```
{
    "id": "4",
    "game": {
        "total_kills": 14,
        "players": [
            "Zeh",
            "Isgalamido",
            "Zeh",
            "Assasinu Credi"
        ],
        "kills": {
            "Zeh": 0,
            "Isgalamido": 2,
            "Assasinu Credi": 1
        }
    }
}
```
