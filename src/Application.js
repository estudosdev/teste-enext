class Application {
  constructor({ httpServer }) {
    this.httpServer = httpServer;
  }

  initialize() {
    this.httpServer.start();
  }
}

module.exports = Application;
