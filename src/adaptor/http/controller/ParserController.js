const httpStatus = require('http-status');
const configFileLogs = require('src-res/config/file');
const PlayerRepository = require('src/adaptor/repository/Player');
const GameRepository = require('src/adaptor/repository/Game');
const DomainParser = require('src/domain/Parser');
const DomainService = require('src/domain/services/Parser');
const OperationParser = require('src/application/Parser');

const newPlayerRepository = new PlayerRepository();
const newGameRepository = new GameRepository();
const newDomainParser = new DomainParser();
const newDomainService = new DomainService(newDomainParser, newGameRepository, newPlayerRepository);
const newOperationParser = new OperationParser(configFileLogs, newDomainService);

class ParserController {
  getAll(_req, res) {
    const games = newOperationParser.getAll();

    return res.status(httpStatus.OK).json({ games: [games] });
  }

  getById(req, res) {
    const { id } = req.params;
    const game = newOperationParser.getGameById(Number(id));
    if (!game) {
      return res.status(httpStatus.NOT_FOUND).json({
        error: 'Game not found',
        typeError: 'NOT_FOUND',
      });
    }
    return res.status(httpStatus.OK).json({
      id,
      game,
    });
  }
}

module.exports = ParserController;
