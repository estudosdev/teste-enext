/* eslint-disable no-underscore-dangle */
const httpStatus = require('http-status');
const ParserController = require('src/adaptor/http/controller/ParserController');
const {
  createHttpMocksToParserController,
} = require('./support');

describe('ADAPTOR :: HTTP :: CONTROLLER :: PARSERCONTROLLER', () => {
  let parserController;
  beforeAll((done) => {
    parserController = new ParserController();
    done();
  });
  describe('When SUCCESS', () => {
    const id = 5;
    const { request, response } = createHttpMocksToParserController(id);
    test('Should returns a game and statusCode 200', () => {
      const responseGameById = parserController.getById(request, response);
      const { id: gameId, game } = responseGameById._getJSONData();
      expect(gameId).toBeDefined();
      expect(gameId).toBe(id);
      expect(game).toBeDefined();
      expect(game.total_kills).toBeDefined();
      expect(game.players).toBeDefined();
      expect(game.kills).toBeDefined();
      expect(response.statusCode).toBe(httpStatus.OK);
    });
  });

  describe('When not found game', () => {
    const id = 99999999999999999999999999999999999999999;
    const { request, response } = createHttpMocksToParserController(id);
    test('Should return zero game and statusCode 404', () => {
      const responseGameById = parserController.getById(request, response);
      const { error, typeError } = responseGameById._getJSONData();
      expect(error).toBe('Game not found');
      expect(typeError).toBe('NOT_FOUND');
    });
  });
});
