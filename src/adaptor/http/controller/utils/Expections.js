class RequiredParamsError extends Error {
  constructor(message) {
    super();
    this.name = 'REQUIRED_PARAM_ERROR';
    this.message = message;
  }
}

module.exports = {
  RequiredParamsError,
};
