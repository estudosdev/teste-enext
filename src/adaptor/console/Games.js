/* eslint-disable no-console */
const createOperationParser = require('./Factory');

const operationParser = createOperationParser();

const listAllGames = new Promise((resolve, reject) => {
  const games = operationParser.getAll();
  if (!games) {
    reject(games);
  }
  resolve(games);
});

listAllGames
  .then(games => console.log(games))
  .catch(err => err);

module.exports = listAllGames;
