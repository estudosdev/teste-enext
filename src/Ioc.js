const { createContainer, asClass, asValue } = require('awilix');

const container = createContainer();

/* Configurations */
const configHttpServer = require('../src-res/config/httpServer');
/* Configurations */

/* Adaptadors */
const HttpServer = require('./adaptor/http/Server');
/* Adaptadors */

/* Application */
const Application = require('./Application');
/* Application */


container.register({
  configHttpServer: asValue(configHttpServer),

  httpServer: asClass(HttpServer),

  application: asClass(Application),
});

module.exports = container;
