class ParserService {
  constructor(domainParser, gameRepository, playerRepository) {
    this.domainParser = domainParser;
    this.gameRepository = gameRepository;
    this.playerRepository = playerRepository;
  }

  getCurrentGame() {
    const { currentGame } = this.domainParser;
    return this.domainParser.games.get(currentGame);
  }

  getGames() {
    return this.domainParser.games;
  }

  getRankedPlayers() {
    const { players } = this.domainParser;
    return Object.entries(players).sort((a, b) => players[a] > players[b]);
  }

  getGameById(id) {
    return this.domainParser.games.get(id);
  }

  newGame(line) {
    const newGame = this.gameRepository.add(line);
    this.domainParser.incrementCurrentGame();
    const { currentGame } = this.domainParser;
    this.domainParser.games.set(currentGame, newGame);
    return newGame;
  }

  newPlayer(line) {
    return this.playerRepository.add(line);
  }

  addNewPlayerInTheGame(newPlayer) {
    const currentGame = this.getCurrentGame();
    const players = currentGame.newPlayer(newPlayer);
    return players;
  }

  updatePlayer(line) {
    const currentGame = this.getCurrentGame();
    const playerId = this.playerRepository.getPlayerId(line);
    const player = currentGame.getPlayerById(playerId);
    return player ? player.updateUsername(line) : player;
  }

  killPlayer(line) {
    this.playerRepository.kill(this, line);
  }

  switchRanking(players = {}) {
    if (players.Isgalamido) {
      this.domainParser.players.Isgalamido += players.Isgalamido;
    } else if (players.Oootsimo) {
      this.domainParser.players.Oootsimo += players.Oootsimo;
    } else if (players.Zeh) {
      this.domainParser.players.Zeh += players.Zeh;
    } else if (players['Dono da Bola']) {
      this.domainParser.players['Dono da Bola'] += players['Dono da Bola'];
    } else if (players['Assasinu Credi']) {
      this.domainParser.players['Assasinu Credi'] += players['Assasinu Credi'];
    }
  }
}

module.exports = ParserService;
