const httpMocks = require('node-mocks-http');

const createHttpMocksToParserController = (id) => {
  const request = httpMocks.createRequest({
    method: 'GET',
    params: {
      id,
    },
  });
  const response = httpMocks.createResponse();

  return {
    request,
    response,
  };
};

module.exports = {
  createHttpMocksToParserController,
};
